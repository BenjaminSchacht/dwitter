from django.shortcuts import render, reverse
from django.http import HttpResponse, HttpResponseRedirect
from dwitter.models import *
from dwitter.forms import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
import re


def signup_view(request):

    form = SignupForm(None or request.POST)
    
    if form.is_valid():
        data = form.cleaned_data
        user = User.objects.create_user(
            data['username'], data['email'], data['password'])
        DwitterUser.objects.create(
            user = user,
            name = data['username']
        )
        login(request, user)
        Notifications.objects.create(
            count = 0,
            user = DwitterUser.objects.filter(name=request.user).first()
        )
        return HttpResponseRedirect(reverse('homepage'))

    return render(request, 'signup.html', {'form': form})


def login_view(request):

    form = LoginForm(None or request.POST)

    if form.is_valid():
        data = form.cleaned_data
        user = authenticate(username=data['username'], password=data['password'])
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse('homepage'))

    return render(request, 'login.html', {'form': form})

@login_required
def homepage_view(request):

    dweets = []
    current_user = DwitterUser.objects.filter(name=request.user)
    followees = current_user.first().users_being_followed.all()
    followees |= current_user
    for followee in followees:
        dweets += Dweet.objects.filter(user=followee)

    notifs = Notifications.objects.filter(user=current_user.first())
    notif_count = notifs.first().count

    return render(request, 'homepage.html', {'data':dweets,
                                            'notifs': notif_count,
                                            'user':current_user.first()})


def send_dweet_view(request):

    if request.method == 'POST':

        form = DweetForm(request.POST)

        if form.is_valid():
            data = form.cleaned_data
            Dweet.objects.create(
                dweet=data['dweet'],
                user=DwitterUser.objects.filter(user=request.user).first()
            )
            at = re.findall("@\S+", data['dweet'])
            if at:
                menchie = at[0]
                mentioned_user = DwitterUser.objects.filter(name=menchie[1:]).first()
                if mentioned_user:
                    user_notifs = Notifications.objects.filter(user=mentioned_user).first()
                    user_notifs.count += 1
                    user_notifs.save()

    else:

        form = DweetForm()

    return render(request, 'send_dweet.html', {'form': form})


def user_view(request, name):

    if request.method == 'POST' and 'follow' in request.POST:
        current_user = DwitterUser.objects.get(name=request.user)
        user_to_be_followed = DwitterUser.objects.get(name=name)
        current_user.users_being_followed.add(user_to_be_followed)
        user_to_be_followed.users_following_me.add(current_user)
    elif request.method == 'POST' and 'unfollow' in request.POST:
        current_user = DwitterUser.objects.get(name=request.user)
        user_to_be_unfollowed = DwitterUser.objects.get(name=name)
        current_user.users_being_followed.remove(user_to_be_unfollowed)
        user_to_be_unfollowed.users_following_me.remove(current_user)

    result = DwitterUser.objects.get(name=name)
    dweets = Dweet.objects.filter(user=result)
    dweet_count = dweets.count()
    current_user= DwitterUser.objects.get(name=request.user)
    following_count = current_user.users_being_followed.count()

    return render(request, 'user.html', {'data': result,
                                        'dweets': dweets,
                                        'count': dweet_count,
                                        'following_count': following_count})


def dweet_view(request, id):

    dweet = Dweet.objects.get(id=id)

    return render(request, 'dweet.html', {'data': dweet})

def notification_view(request):

    current_user = DwitterUser.objects.filter(name=request.user)
    notifs = Notifications.objects.filter(user=current_user.first())
    notif_count = notifs.first().count


    current_notifs = notifs.first()
    current_notifs.count = 0
    current_notifs.save()

    return render(request, 'notification.html', {'data': notif_count})