from django.db import models
from django.contrib.auth.models import User

class DwitterUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    followees = models.ManyToManyField('self', related_name='users_being_followed',
                                        symmetrical=False, blank=True)
    followers = models.ManyToManyField('self', related_name='users_following_me',
                                        symmetrical=False, blank=True)

    def __str__(self):
        return self.name

class Dweet(models.Model):
    dweet = models.CharField(max_length=140)
    user = models.ForeignKey(DwitterUser, on_delete=models.CASCADE)

    def __str__(self):
        return self.dweet

class Notifications(models.Model):
    count = models.IntegerField()
    user = models.ForeignKey(DwitterUser, on_delete=models.CASCADE)

    def __int__(self):
        return self.count