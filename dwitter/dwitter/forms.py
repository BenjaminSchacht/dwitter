from django import forms
from dwitter.models import DwitterUser

class DweetForm(forms.Form):
    dweet = forms.CharField(max_length=140)

class SignupForm(forms.Form):
    username = forms.CharField(max_length=50)
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput())

class LoginForm(forms.Form):
    username = forms.CharField(max_length=50)
    password = forms.CharField(widget=forms.PasswordInput())